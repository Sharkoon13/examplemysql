
#include <gtest/gtest.h>

inline std::array<unsigned char, 20> toArray(const uint32_t devhint, const uint32_t objId, const uint32_t codekey) {
    std::array<unsigned char, 20> logdataBuff{};
    std::fill(logdataBuff.begin(), logdataBuff.end(), 0);
    logdataBuff[0] = 0xFE;  //constant 0xFE06    //0xfe06
    logdataBuff[1] = 0x06;  //constant 0xFE06
    logdataBuff[2] = (devhint & 0xFF00) >> 2;  //DEVHINT  //0x000b
    logdataBuff[3] = devhint & 0xFF;          //DEVHINT
    logdataBuff[4] = 0x02;                          //DIRECTION  0x02
    logdataBuff[5] = 0x03;                          //BASE DECISION  0x03
    {
        const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(objId));
        const unsigned char *end = begin + sizeof(objId);
        unsigned long idx = 6 + sizeof(objId);
        for (auto *it = begin; it != end; ++it) {
            logdataBuff[--idx] = *it;
        }
    }
    {
        const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(codekey));
        const unsigned char *end = begin + sizeof(codekey);
        unsigned long idx = 10 + sizeof(codekey);
        for (auto *it = begin; it != end; ++it) {
            logdataBuff[--idx] = *it;
        }
    }

    logdataBuff[18] = 0xFF;   //end HEX by 0x FF FF
    logdataBuff[19] = 0xFF;

    return logdataBuff;
}

TEST(logdata, serializeCodeKey) {
    std::array<unsigned char, 20> rightArray{0xfe, 0x06, 0x00, 0x0b, 0x02, 0x03, 0x00, 0x00, 0x32, 0x37, 0x18, 0x26,
                                             0x2D, 0xBB, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff};

    uint32_t m_currentStep = 11;  //devhint
    uint32_t m_idx = 12855;
    uint32_t m_codekey = 405155259;

    auto logdataBuff = toArray(m_currentStep, m_idx, m_codekey);
    ASSERT_TRUE(std::equal(logdataBuff.begin(), logdataBuff.end(), rightArray.begin(), rightArray.end()));

}

TEST(logdata, serialize) {
    {  //fill prepared statement

        std::array<unsigned char, 20> rightArray{0xfe, 0x06, 0x00, 0x0b, 0x02, 0x03, 0x00, 0x00, 0x32, 0x37, 0x18, 0x87,
                                                 0x0d, 0x84, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff};

        uint32_t m_currentStep = 11;  //devhint
        uint32_t m_idx = 12855;
        uint32_t m_codekey = 411504004;

        auto logdataBuff = toArray(m_currentStep, m_idx, m_codekey);

//        std::array<unsigned char, 20> logdataBuff{};
//        std::fill( logdataBuff.begin(), logdataBuff.end() , 0 );
//
//        logdataBuff[0] = 0xFE;  //constant 0xFE06    //0xfe06
//        logdataBuff[1] = 0x06;  //constant 0xFE06
//        logdataBuff[2] = (m_currentStep & 0xFF00)>> 2;  //DEVHINT  //0x000b
//        logdataBuff[3] = m_currentStep & 0xFF;          //DEVHINT
//        logdataBuff[4] = 0x02;                          //DIRECTION  0x02
//        logdataBuff[5] = 0x03;                          //BASE DECISION  0x03
//        {
//            const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(m_idx));
//            const unsigned char *end = begin + sizeof(m_idx);
//            unsigned long idx = 6 + sizeof(m_idx);
//            for ( auto* it = begin ; it != end; ++it ) {
//                logdataBuff[--idx] = *it;
//            }
//        }
//
////        logdataBuff[6] = (m_idx & 0xFF000000) >> 6;     //OBJ_ID (PERSONAL.ID) 0x00003237
////        logdataBuff[7] = (m_idx & 0xFF0000) >> 4;       //OBJ_ID (PERSONAL.ID) 0x00003237
////        logdataBuff[8] = (m_idx & 0xFF00) >> 2;         //OBJ_ID (PERSONAL.ID) 0x00003237
////        logdataBuff[9] = m_idx & 0xFF;                  //OBJ_ID (PERSONAL.ID) 0x00003237
//        {
//            const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(m_codekey));
//            const unsigned char *end = begin + sizeof(m_codekey);
//            unsigned long idx = 10 + sizeof(m_codekey);
//            for ( auto* it = begin ; it != end; ++it ) {
//                logdataBuff[--idx] = *it;
//            }
//        }
//
////        logdataBuff[10] = (m_codekey & 0xFF000000) >> 6;//PERSONAL.CODEKEY 0x 18 87 0d 84 00 00 00 00
////        logdataBuff[11] = (m_codekey & 0xFF0000) >> 4;  //PERSONAL.CODEKEY 0x 18 87 0d 84 00 00 00 00
////        logdataBuff[12] = (m_codekey & 0xFF00) >> 2;    //PERSONAL.CODEKEY 0x 18 87 0d 84 00 00 00 00
////        logdataBuff[13] = m_codekey & 0xFF;             //PERSONAL.CODEKEY 0x 18 87 0d 84 00 00 00 00
//        //all after - filled by zero
//
//        logdataBuff[18] = 0xFF;   //end HEX by 0x FF FF
//        logdataBuff[19] = 0xFF;
//        //TODO: compare logdata values and this variant.


        ASSERT_TRUE(std::equal(logdataBuff.begin(), logdataBuff.end(), rightArray.begin(), rightArray.end()));
    }
}

