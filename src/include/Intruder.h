//
// Created by iddqd13 on 3/15/18.
//

#ifndef AGENTCASTLEGENERATOR_INTRUDER_H
#define AGENTCASTLEGENERATOR_INTRUDER_H

#include <functional>
#include <IWalkerBase.h>

namespace Castle {
    /**
     * @class Intruder
     * @brief Intruder opens boxes or sets them on fire (write fake records!!! )
     */
    class Intruder : public IWalkerBase {
        typedef std::tuple<uint32_t, std::string> AlarmLine;
        typedef std::forward_list<AlarmLine> AlarmLinesList;
        typedef std::reference_wrapper<const MySQL::DbConnectionPtr> ConnectionPtrRef;
    public:
        explicit Intruder(DeviceSdk::ILogger *logger = nullptr);

        virtual ~Intruder() = default;

        /**
         * Start detached thread with walking to zones
         * @param walkingPath forward_list with Access Point Identificators.
         * @param baseConnectionPtr clone of original Db connection
         * !!!!Warning! subthread take ownership of baseConnectionPtr!
         * calling this method must be like : p->startWalking(path , conn->clone() );
         */
        virtual void
        startWalking(const AccessPointPath &walkingPath, MySQL::DbConnectionPtr baseConnectionPtr) override;

        inline AlarmLinesList retrieveAlarmLines(ConnectionPtrRef connPtr);

        inline void changeAlarmLines(ConnectionPtrRef connPtr, AlarmLinesList &list, uint32_t state);

        inline void changeDeviceState(ConnectionPtrRef connPtr, uint32_t devId, uint state, uint8_t addStatus = 0);
    };

}
#endif //AGENTCASTLEGENERATOR_INTRUDER_H
