//
// Created by iddqd13 on 3/15/18.
//

#include <Intruder.h>
#include <tuple>
#include <utils.h>

namespace Castle {

    using namespace utils;

    Intruder::Intruder(DeviceSdk::ILogger *logger) : IWalkerBase(logger) {

    }


    void Intruder::startWalking(const AccessPointPath &walkingPath,
                                MySQL::DbConnectionPtr baseConnectionPtr) {

        {
            std::lock_guard<std::mutex> lockGuard(m_mutex);
            m_walking = true;
        }

        m_path = walkingPath;
        m_currentStep = m_path.begin();

        m_thread = std::thread([this, baseConnectionPtr = std::move(baseConnectionPtr)]() {
            bool run = m_walking;
            try {

                ///get list of alarmlines
                auto alarmLines = retrieveAlarmLines(std::ref(baseConnectionPtr));

                while (run) {
                    ///warm up
                    std::this_thread::sleep_for(std::chrono::seconds(30));

                    ///set state of all alarmlines to ALARM!!!
                    changeAlarmLines(std::ref(baseConnectionPtr), alarmLines, toUInt(AlarmLineState::ALARM));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::minutes(2));

                    ///change state of all alarm lines to Active
                    changeAlarmLines(std::ref(baseConnectionPtr), alarmLines, toUInt(AlarmLineState::ACTIVE));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///change device state to BoxOpened
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::boxOpened));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///change device state to BoxClosed
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::boxClosed));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///change device state to fireunlock begin!
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::fireUnlockBegin));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///change device state to fireunlock end.
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::fireUnlockEnd));

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///disconnect device on logs ( status - offline )
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::apOnlineStatus), 0);

                    ///relax
                    std::this_thread::sleep_for(std::chrono::seconds(10));

                    ///connect device on logs ( status - online )
                    changeDeviceState(std::ref(baseConnectionPtr), m_currentStep->first,
                                      toUInt(LogDataType::apOnlineStatus), 1);

                    ++m_currentStep;
                    if (m_currentStep == m_path.end()) {
                        m_currentStep = m_path.begin();

                        SMART_LOG_DEBUG(SL("[%s]Intruder goes to the next loop!"), baseConnectionPtr->uri().c_str());
                    }

                    {
                        std::lock_guard<std::mutex> lockGuard(m_mutex);
                        run = m_walking;
                    }
                }

            } catch (sql::SQLException &e) {
                /*
                  MySQL Connector/C++ throws three different exceptions:

                  - sql::MethodNotImplementedException (derived from sql::SQLException)
                  - sql::InvalidArgumentException (derived from sql::SQLException)
                  - sql::SQLException (derived from std::runtime_error)
                */
                SMART_LOG_ERROR(SL("error: %s , Error code : %d , SQLState : %s"), e.what(), e.getErrorCode(),
                                e.getSQLStateCStr());
            } catch (std::exception &e) {
                SMART_LOG_ERROR(SL("%s"), e.what());
            }
        });


    }

    Intruder::AlarmLinesList Intruder::retrieveAlarmLines(ConnectionPtrRef connPtr) {
        AlarmLinesList alarmLines{};
        { //get alarmlines for damage
            auto stmt = connPtr.get()->createStatement();
            auto res = MySQL::Ptr::WrapUniq(stmt->executeQuery("select id,name from `tc-db-main`.alarmlines;"));
            while (res->next()) {
                alarmLines.emplace_front(res->getUInt(1), res->getString(2));
            }
        }
        return alarmLines;
    }

    void
    Intruder::changeAlarmLines(ConnectionPtrRef connPtr, Intruder::AlarmLinesList &list,
                               uint32_t state) {
        auto preparedStmt = connPtr.get()->prepareStatement(MySQL::ALARMLOG_INSERT_STATEMENT);

        for (auto &aline : list) {  //fill prepared statement
            preparedStmt->setDateTime(toUInt(AlarmLogCols::LOGTIME), currentTime());
            preparedStmt->setInt(toUInt(AlarmLogCols::LINEID), std::get<0>(aline));
            preparedStmt->setInt(toUInt(AlarmLogCols::NEWSTATE), state);
            preparedStmt->setInt64(toUInt(AlarmLogCols::FRAMETS), 0);

            preparedStmt->execute();

            SMART_LOG_DEBUG(SL("[%s]Intruder set %s alarm %u"),
                            connPtr.get()->uri().c_str(), std::get<1>(aline).c_str(), state);
        }

    }

    void Intruder::changeDeviceState(ConnectionPtrRef connPtr, uint32_t devId, uint state, uint8_t addStatus) {
        auto preparedStmt = connPtr.get()->prepareStatement(MySQL::LOGS_INSERT_STATEMENT);
        {  //fill prepared statement

            std::vector<unsigned char> data;
            auto castedState = static_cast<LogDataType>(state);
            const LogDataType toState = castedState;

            if (toState == LogDataType::fireUnlockBegin || toState == LogDataType::fireUnlockEnd) {

                auto logdataBuff = toArray<4>(toState, devId);
                data.insert(data.begin(), logdataBuff.begin(), logdataBuff.end());
            } else if (toState == LogDataType::boxOpened || toState == LogDataType::boxClosed) {
                auto logdataBuff = toArray<5>(toState, devId);
                data.insert(data.begin(), logdataBuff.begin(), logdataBuff.end());
            } else if (toState == LogDataType::apOnlineStatus) {
                auto logdataBuff = toArray<5>(toState, devId, addStatus);
                data.insert(data.begin(), logdataBuff.begin(), logdataBuff.end());
            }


            membuf sbuf(reinterpret_cast<char *>(data.data()),
                        reinterpret_cast<char *>(data.data() + data.size()));
            std::istream istr(&sbuf);

            preparedStmt->setDateTime(toUInt(LogsCols::LOGTIME), currentTime());
            preparedStmt->setInt(toUInt(LogsCols::AREA), NULL_VAL);
            preparedStmt->setBlob(toUInt(LogsCols::LOGDATA), &istr);
            preparedStmt->setInt(toUInt(LogsCols::EMPHINT), NULL_VAL);
            preparedStmt->setInt(toUInt(LogsCols::DEVHINT), devId);
            preparedStmt->setInt(toUInt(LogsCols::FRAMETS), NULL_VAL);

            preparedStmt->execute();

            SMART_LOG_DEBUG(SL("[%s]Access Point %d change state to %d"),
                            connPtr.get()->uri().c_str(), devId, state);
        }
    }
}