//
// Created by iddqd13 on 3/14/18.
//

#include <CustomPersonInfo.h>
#include <DeviceSdk/ILogger.h>
#include <Helpers/Log.h>

#include <utils.h>
#include <inttypes.h>

namespace Castle {
    std::vector<CustomPersonInfo>
    retriveFromDb(const unsigned int count, MySQL::DbConnectionPtr baseConnectionPtr, DeviceSdk::ILogger *logger) {

        auto getLogger = [logger]() { return logger; };

        std::vector<CustomPersonInfo> infos;

        try {
            auto preparedStmt = baseConnectionPtr->prepareStatement(
                    "select id , name , pos , HEX(codekey) , exptime , firedtime "
                            "from  `tc-db-main`.personal "
                            "where firedtime is null and exptime > (?) and STATUS = 'AVAILABLE' and "
                            "substr(codekey,1,1)!=0x00  and length(codekey) = 8 "
                            "order by id desc "
                            "limit 100; ");

            preparedStmt->setDateTime(1, utils::currentTime(-1));
//            preparedStmt->setInt( 2 , count );

            auto result = MySQL::Ptr::WrapUniq(preparedStmt->executeQuery());

            for (unsigned int i = 0; i < count; ++i) {
                if (!result->next()) {
                    break;
                }

                uint32_t id = result->getUInt(1);
                std::string name = result->getString(2);
                uint64_t codekey{};
                {
                    std::string codekeyHex = result->getString(4);
                    {
                        auto pos = codekeyHex.rfind("00000000");
                        if (pos == 8) {   //first 8 symbols are , for example, 18262DBB
                            codekeyHex.erase(pos);
                        }
                    }
                    codekey = std::stoull(codekeyHex, nullptr, 16);
                    SMART_LOG_DEBUG(SL("get hex: |%s| , get key : %"
                                            PRIu64), codekeyHex.c_str(), codekey);
                }
                infos.emplace_back(id, name, codekey);
            }
        } catch (sql::SQLException &e) {
            SMART_LOG_ERROR(SL("error: %s , Error code : %d , SQLState : %s"), e.what(), e.getErrorCode(),
                            e.getSQLStateCStr());
        } catch (std::exception &e) {
            SMART_LOG_ERROR(SL("%s"), e.what());
        }

        return infos;
    }

}
