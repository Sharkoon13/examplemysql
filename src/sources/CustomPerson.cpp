//
// Created by iddqd13 on 3/13/18.
//

#include <CustomPerson.h>
#include <utils.h>


namespace Castle {




    CustomPerson::CustomPerson(const std::string &name, uint32_t idx, uint64_t codekey, DeviceSdk::ILogger *logger)
            : IWalkerBase(logger), m_name{name}, m_idx{idx},
              m_codekey{codekey} {

    }

    void CustomPerson::startWalking(const AccessPointPath &walkingPath, MySQL::DbConnectionPtr baseConnectionPtr) {

        {
            std::lock_guard<std::mutex> lockGuard(m_mutex);
            m_walking = true;
        }

        using namespace utils;

        m_path = walkingPath;
        m_currentStep = m_path.begin();

        m_thread = std::thread([this, baseConnectionPtr = std::move(baseConnectionPtr)]() {
            bool run = m_walking;
            try {

                auto preparedStmt = baseConnectionPtr->prepareStatement(MySQL::LOGS_INSERT_STATEMENT);
                while (run) {
                    std::this_thread::sleep_for(std::chrono::seconds(30));


                    {  //fill prepared statement
                        auto logdataBuff = toArray(m_currentStep->first, m_currentStep->second, m_idx,
                                                   m_codekey);

                        membuf sbuf(reinterpret_cast<char *>(logdataBuff.begin()),
                                    reinterpret_cast<char *>(logdataBuff.end()));
                        std::istream istr(&sbuf);

                        preparedStmt->setDateTime(toUInt(LogsCols::LOGTIME), currentTime());
                        preparedStmt->setInt(toUInt(LogsCols::AREA), NULL_VAL);
                        preparedStmt->setBlob(toUInt(LogsCols::LOGDATA), &istr);
                        preparedStmt->setInt(toUInt(LogsCols::EMPHINT), m_idx);
                        preparedStmt->setInt(toUInt(LogsCols::DEVHINT), m_currentStep->first);
                        preparedStmt->setInt(toUInt(LogsCols::FRAMETS), NULL_VAL);

                        preparedStmt->execute();

                        SMART_LOG_DEBUG(SL("[%s]Walker %s was on Access Point %d with direction %d"),
                                        baseConnectionPtr->uri().c_str(), m_name.c_str(), m_currentStep->first,
                                        m_currentStep->second);
                    }
                    ++m_currentStep;
                    if (m_currentStep == m_path.end()) {
                        m_currentStep = m_path.begin();

                        SMART_LOG_DEBUG(SL("[%s]Walker %s goes to the next loop!"),
                                        baseConnectionPtr->uri().c_str(), m_name.c_str());
                    }

                    {
                        std::lock_guard<std::mutex> lockGuard(m_mutex);
                        run = m_walking;
                    }
                }

            } catch (sql::SQLException &e) {
                /*
                  MySQL Connector/C++ throws three different exceptions:

                  - sql::MethodNotImplementedException (derived from sql::SQLException)
                  - sql::InvalidArgumentException (derived from sql::SQLException)
                  - sql::SQLException (derived from std::runtime_error)
                */
                SMART_LOG_ERROR(SL("error: %s , Error code : %d , SQLState : %s"), e.what(), e.getErrorCode(),
                                e.getSQLStateCStr());
            } catch (std::exception &e) {
                SMART_LOG_ERROR(SL("%s"), e.what());
            }
        });
    }


}

