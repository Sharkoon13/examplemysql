//
// Created by iddqd13 on 3/13/18.
//

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <regex>

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include <DbConnection.h>

#include <App.h>
#include <boost/asio.hpp>


static void handler(const boost::system::error_code &error, int signal_number) {
//    //std::cout << "handling signal " << signal_number << std::endl;
//    //exit(1);
}

static void wait() {
    boost::asio::io_service io_service;
    boost::asio::signal_set signals(io_service, SIGINT);
    signals.async_wait(handler);
    io_service.run();
}

int
main(int argc, char *argv[]) {

    DeviceSdk::LogLevel level{DeviceSdk::LOG_Debug};

    if (argc < 4) {
        std::cout << "Error. Missing arguments (Loglevel, walkersCount and connects to databases. ) " << std::endl
                  << "For launching applicaiton need specify db connection" << std::endl
                  << "LOG_LEVEL WALKERCOUNT db://username:password@hostIpOrName/mysql/DbName/charset" << std::endl
                  << "for example: " << std::endl
                  << "100 3 db://root:pass@10.20.30.253/mysql/tc-db-main/utf8" << std::endl;
        return 0;
    }

    level = static_cast<DeviceSdk::LogLevel> ( atoi(argv[1]));

    uint32_t walkersCount = static_cast<uint32_t >( atoi(argv[2]));

    std::forward_list<std::string> dbConns;

    for (int i = 3; i < argc; ++i) {
        dbConns.emplace_front(argv[i]);
    }

    auto app = std::make_unique<Castle::App>(dbConns, level);

    app->trace(("Start Generator with log level + " + std::to_string(level) + " and walkers count = " +
                std::to_string(walkersCount)).c_str());
    app->start(walkersCount);

    wait();

    return 0;
}