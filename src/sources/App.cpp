//
// Created by iddqd13 on 3/14/18.
//

#include <App.h>
#include <CustomPerson.h>
#include <Intruder.h>
#include <CustomPersonInfo.h>

namespace Castle {

    App::App(const std::forward_list<std::string> &uris, DeviceSdk::LogLevel level) {
        m_logger = std::make_unique<DeviceSdk::Logger>(level);

        for (auto &uri : uris) {
            try {
                m_connections.emplace(uri.c_str());
            } catch (std::exception &e) {
                SMART_LOG_ERROR(SL("while processing uri : %s, get error : %s"), uri.c_str(), e.what());
            }
        }
        SMART_LOG_DEBUG(SL("Available Connections:"));
        for (auto &conn : m_connections) {
            auto uri = conn.uri();
            SMART_LOG_DEBUG(SL("\t %s"), conn.uri().c_str());
        }
    }

    void App::start(unsigned int countOfWalkers) {

        for (auto &connection : m_connections) {  //for each connection|database

            SMART_LOG_DEBUG(SL("gettings walkers info from connection %s "), connection.uri().c_str());

            auto walkers = retriveFromDb(countOfWalkers, connection.clone(), getLogger());

            if (walkers.empty()) {
                SMART_LOG_ERROR(SL("Error while getting walkers on DB %s, don't create anything"),
                                connection.uri().c_str());
            }
            if (countOfWalkers > walkers.size())
                countOfWalkers = walkers.size();

            AccessPointPath app = {{11, 2},
                                   {5,  2},
                                   {17, 2},
                                   {18, 1},
                                   {14, 1},
                                   {12, 1}};  //big circle

            for (unsigned int i = 0; i < countOfWalkers; ++i) {  //create a few walkers.
                auto &walker = walkers[i];
                m_walkers.push_back(
                        std::make_unique<CustomPerson>(walker.name, walker.id, walker.codekey, getLogger()));
                m_walkers.back()->startWalking(app, connection.clone());
            }

            m_walkers.push_back(
                    std::make_unique<Intruder>(getLogger()));
            m_walkers.back()->startWalking(app, connection.clone());
        }
    }


    void App::trace(const char *str) {
        SMART_LOG_TRACE(SL(str));
    }
}