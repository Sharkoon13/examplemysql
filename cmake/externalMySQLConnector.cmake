CMAKE_MINIMUM_REQUIRED(VERSION 3.5.0)

include(ExternalProject)

set(MYSQLCONN_TMPDIR ${CMAKE_BINARY_DIR}/MySQL_tmp)
set(MYSQLCONN_DIR ${CMAKE_BINARY_DIR}/MySQL)

set(MYSQLCONN_INCLUDE_DIRS ${MYSQLCONN_DIR}/include)
set(MYSQLCONN_LIBRARY_STATIC ${MYSQLCONN_DIR}/lib/libmysqlcppconn-static.a)
set(MYSQLCONN_LIBRARY_DYNAMIC ${MYSQLCONN_DIR}/lib/mysqlcppconn)
set(MYSQLCONN_LIBRARY_DIR ${MYSQLCONN_DIR}/lib)


file(MAKE_DIRECTORY ${MYSQLCONN_DIR} ${MYSQLCONN_TMPDIR})

ExternalProject_Add(
        MySQLConnector_get
        URL https://dev.mysql.com/get/Downloads/Connector-C++/mysql-connector-c++-1.1.9-linux-ubuntu16.04-x86-64bit.tar.gz
        URL_MD5 5d0de006af31be8f4fcc400b0f2350dd
        DOWNLOAD_DIR ${MYSQLCONN_DIR}
        SOURCE_DIR ${MYSQLCONN_DIR}
        PREFIX ${MYSQLCONN_TMPDIR}
        UPDATE_COMMAND ""
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
)

